﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Drawing;

namespace AseComp1
{
    class Circle : Shape
    {
        //radius declaration
        int radius;


        public Circle() : base()
        {

        }
        //parameterized constructor
        public Circle(int x, int y, int radius) : base(x, y)
        {
            this.radius = radius;//the only thing that is different from shape
        }

        //draw method
        public void setRadius(int radius)
        {
            this.radius = radius;
        }

        public int getRadius()
        {
            return this.radius;
        }

        public override void draw(Graphics g, Color color, int thickness)
        {
            Pen p = new Pen(color,thickness);
            g.DrawEllipse(p, x, y, radius, radius);
            
        }
    }
}
