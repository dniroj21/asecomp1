﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;


namespace AseComp1
{
    public partial class FrmGPLA : Form
    {
        //declaration of variable for shapes
        Circle circle;
        Rectangle rectangle;
        Polygon polygon;
        //end of variable declaration

        Boolean drawCircle, drawRectangle, drawPolygon, drawLine;//boolean values for checking shapes
        String program;//string to hold textarea info
        String[] words;//words of individual program
        int moveX, moveY;//cursor moving direction points
        int thickness;//thickness of pen
       
       

        Color color;
        Point point;//point on drawaing panel
        string actionCmd;
        string consoletext;

        Shape shape1, shape2;//shapefactory declaration
        //Individual object list for shapes
        List<Circle> circleObjects;
        List<Rectangle> rectangleObjects;
        List<Polygon> polygonObjects;
        List<MoveDirection> moveObjects;

      

        public FrmGPLA()
        {
            InitializeComponent();
            //Shape Creation and initialization
            AbstractFactory shapeFactory = FactoryProducer.getFactory("Shape");
            shape1 = shapeFactory.getShape("Circle");
            shape2 = shapeFactory.getShape("Rectangle");
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("For Your Helpline:\n" +
               "draw circle 100\n" +
               "draw rectangle 100 50\n" +
               "draw polygon\n" +
               "moveto 100 90\n" +
               "color green 2\n");
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, txtCodeArea.Text);
            }
        }

       
       
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //opens dialogbox when button is cliced to load the code
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtCodeArea.Text = File.ReadAllText(openFileDialog1.FileName);
            }
        }

      

        private void FrmGPLA_Load(object sender, EventArgs e)
        {
            circle = new Circle(); //instantiating circle
            circleObjects = new List<Circle>();// declaration of array of circle objects
            rectangleObjects = new List<Rectangle>();// declaration of array of rectabgle objects
            polygonObjects = new List<Polygon>();//declaration of array for polygon objects          
            moveObjects = new List<MoveDirection>();//declaration of array for moving objects
            color = Color.Black;
        }

        private void txtExecutionCmd_TextChanged(object sender, EventArgs e)
        {
            actionCmd = txtExecutionCmd.Text.ToLower();
           
            switch (actionCmd)
            {
                case "run":
                    try
                    {
                        program = txtCodeArea.Text.ToLower();
                        char[] delimiters = new char[] { '\r', '\n' };
                        string[] parts = program.Split(delimiters, StringSplitOptions.RemoveEmptyEntries); //holds invididuals code line on the basis of delimiters
                        consoletext = "Program code: \n";
                        foreach (string part in parts)
                        {
                            consoletext += part + "\n";
                        }
                        consoletext += "\n\n";


                        //loop through the whole program code line
                        for (int i = 0; i < parts.Length; i++)
                        {
                            //single code line
                            String code_line = parts[i];

                            char[] code_delimiters = new char[] { ' ' };
                            words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries); //holds invididuals code line

                            //condition to check if "draw" then
                            if (words[0].Equals("draw"))
                            {
                                if (words[1] == "circle") // condition to check if "circle" then
                                {
                                    if (!(words.Length == 3)) //checks if written code is correct or not
                                    {
                                        MessageBox.Show("Enter correct command and parameter");
                                        consoletext += "View the correct Command: \n e.g. draw circle 100 or draw circle r \n\n";
                                    }
                                    else
                                    {
                                        if (circleObjects.Exists(x => x.getX() == moveX && x.getY() == moveY
                                        && x.getRadius() == Convert.ToInt32(words[2])) == true)
                                        //check for the x,y,radius exists or not
                                        {
                                            consoletext += "!!circle already exists with the given value!!\n\n";

                                        }
                                        else
                                        {
                                            //Create a new circle
                                            Circle circle = new Circle();
                                            circle.setX(moveX);
                                            circle.setY(moveY);
                                            circle.setRadius(Convert.ToInt32(words[2]));
                                            circleObjects.Add(circle);
                                            drawCircle = true;
                                            consoletext += "Adding new circle\n\n";
                                        }
                                    }
                                }
                                if (words[1].Equals("rectangle")) 
                                {
                                    //MessageBox.Show(moveX.ToString());
                                    if (!(words.Length == 4)) //extending parameter values
                                    {
                                        MessageBox.Show("Enter correct command");
                                        consoletext += "View the correct Command:: \n e.g. draw rectangle 100 100 or draw circle h w \n\n";
                                    }
                                    else
                                    {
                                        if (rectangleObjects.Exists(x => x.getX() == moveX && x.getY() == moveY
                                        && x.getLength() == Convert.ToInt32(words[2]) && x.getBreadth() ==
                                        Convert.ToInt32(words[3])) == true)//check for the x,y,radius exists or not
                                        {
                                            consoletext += "!!rectangle object exists with given parameters!!\n\n";
                                        }
                                        else
                                        {
                                            //create a new rectangle
                                            Rectangle rect = new Rectangle();
                                            rect.setX(moveX);
                                            rect.setY(moveY);
                                            rect.setLength(Convert.ToInt32(words[2]));
                                            rect.setBreadth(Convert.ToInt32(words[3]));
                                            rectangleObjects.Add(rect);
                                            drawRectangle = true;
                                            consoletext += "Adding new rectangle\n\n";
                                        }
                                    }
                                }

                                else if (words[1].Equals("polygon"))
                                {
                                    drawPolygon = true;
                                }
                            }
                            if (words[0] == "moveto") // condition to check if "move" then
                            {                              
                                  moveX = Convert.ToInt32(words[1]);
                                  moveY = Convert.ToInt32(words[2]);
                                  consoletext += "X=" + moveX + "\n" + "Y=" + moveY + "\n\n";
                            }
                            if (words[0] == "color")
                            {
                                thickness = Convert.ToInt32(words[2]);

                                if (words[1] == "red")
                                {
                                      color = Color.Red;
                                    consoletext += "Pen is of red color\n\n";
                                }
                                else if (words[1] == "blue")
                                {
                                    color = Color.Blue;
                                    consoletext += "Pen is of blue color\n\n";
                                }
                                else if (words[1] == "yellow")
                                {
                                    color = Color.Yellow;
                                    consoletext += "Pen is of yellow color\n\n";
                                }
                                else
                                {
                                    color = Color.Green;
                                    consoletext += "Pen is of green color\n\n";
                                }
                            }
                        }
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        consoletext += "Error: " + ex.Message + "\n\n";
                    }
                    catch (FormatException ex)
                    {
                        consoletext += "!!Please input correct parameter!!\n\n";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        consoletext += "!!Please input correct parameter!!\n\n";
                    }
                    pnlDisplayOutput.Refresh(); //refresh with every drawing equals to true
                    break;
                   
                    case "clear":
                    circleObjects.Clear();
                    rectangleObjects.Clear();
                    moveObjects.Clear();
                    polygonObjects.Clear();
                    this.drawPolygon = false;
                    this.drawCircle = false;
                    this.drawRectangle = false;
                    this.txtCodeArea.Clear();
                    pnlDisplayOutput.Refresh();
                    break;


                   case "reset":
                    moveX = 0;
                    moveY = 0;
                    moveObjects.Clear();
                    break;

                

            }
        }
        private void pnlDisplayOutput_Paint_1(object sender, PaintEventArgs e)
        {
            //graphics to draw in panel
            Graphics g = e.Graphics;
            if (drawCircle == true)//condition to draw circle
            {
                foreach (Circle circleObject in circleObjects)
                {
                    consoletext += "Drawing Circle\n\n";
                    circleObject.draw(g, color, thickness);//with the given graphics draw circle

                }
            }

            // condition to draw rectangle

            if (drawRectangle == true) 
            {
                foreach (Rectangle rectangleObject in rectangleObjects)
                {
                    consoletext += "Drawing Rectangle\n\n";
                    rectangleObject.draw(g, color,thickness );// with the given graphics draw rectangle
                }
            }
            //condition to draw triangle with polygon points
            if (drawPolygon == true)
            {
                Pen blackPen = new Pen(color);
                PointF point1 = new PointF(50.0F, 50.0F);
                PointF point2 = new PointF(170.0F, 150.0F);
                PointF point3 = new PointF(20.0F, 250.0F);
                string[] str = new string[3];
                PointF[] curvePoints =
                {
                    point1,
                    point2,
                    point3
                };
                e.Graphics.DrawPolygon(blackPen, curvePoints);
            }

        }


    }
}