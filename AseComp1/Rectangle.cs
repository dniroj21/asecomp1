﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace AseComp1
{
    class Rectangle : Shape
    {
        ///declaration of variables
        int length, breadth;
        
        /// Parameterized constructor        
        public Rectangle(int x, int y, int length, int breadth) : base(x, y)
        {
            this.length = length;
            this.breadth = breadth;
        }
        /// another parameterized constructor        
        public Rectangle(int x, int y) : base(x, y)
        {

        }
        ///default constructor
        public Rectangle()
        {

        }

        ///<summary>
        ///length setter
        ///</summary>
        ///<param name="length"></param>
        public void setLength(int length)
        {
            this.length = length;
        }
        ///<summary>
        ///length getter
        ///</summary>
        ///<returns></returns>
        public int getLength()
        {
            return this.length;

        }
        ///<summary>
        ///breadth setter
        ///</summary>
        ///<param name="breadth"></param>
        public void setBreadth(int breadth)
        {
            this.breadth = breadth;
        }
        ///<summary>
        ///breadth getter
        ///</summary>
        ///<returns></returns>
        public int getBreadth()
        {
            return this.breadth;

        }

        /// <summary>
        /// draw method
        /// </summary>
        /// <param name="g"></param>
        public override void draw(Graphics g, Color color, int thickness)
        {
            Pen p = new Pen(color, thickness);
            g.DrawRectangle(p, x, y, length, breadth);
        }
    }
}